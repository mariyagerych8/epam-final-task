Feature: Smoke
  As a user
  I want to test all main site functional
  So that I can be sure that site works correctly


  Scenario Outline: Check site main functions
    Given User opens '<mainUrl>' page
    And User checks main logo visibility
    And User checks header visibility
    And User checks topbar visibility
    And User checks footer visibility
    And User checks search field visibility
    And User checks shop women button visibility
    And User checks shop men button visibility
    And User checks account button visibility
    And User checks cart visibility
    And User checks saved Items icon visibility
    And User checks country selector visibility
    When User moves to Account button
    And User checks all items visibility on dropdown list
    And User clicks on shop women button
    Then User checks that current url contains '<floor>'

    Examples:
      | mainUrl               | floor |
      | https://www.asos.com/ | women |


  Scenario Outline: Check product details page
    Given User opens '<mainUrl>' page
    When User clicks on search field
    And User makes search by keyword '<keyword>'
    And User clicks search button
    And User checks Price of product visibility
    And User checks Image of product visibility
    And User checks Star rating visibility
    And User checks Size select visibility
    And User checks Save button visibility
    And User checks Add to bag button visibility
    Then User checks Colour select visibility

    Examples:
      | mainUrl               | keyword  |
      | https://www.asos.com/ | 22603267 |

  Scenario Outline: Check product filters
    Given User opens '<mainUrl>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User checks that current url contains '<searchKeyword>'
    And User sorts products by Price high to low
    And User checks that current url contains '<sortKeyword>'
    And user selects a Gender from the drop-down list
    And User checks that current url contains '<genderKeyword>'
    And User selects a Color from the drop-down list
    Then User checks that current url contains '<colorKeyword>'

    Examples:
      | mainUrl               | keyword | searchKeyword | sortKeyword | genderKeyword | colorKeyword |
      | https://www.asos.com/ | Watches | watches       | pricedesc   | 2001          | 4            |


  Scenario Outline: Check recently viewed products
    Given User opens '<mainUrl>' page
    And User clicks on search field
    And User makes search by keyword '<keyword>'
    And User clicks search button
    When User clicks on search field
    And User makes search by keyword '<keyword2>'
    And User clicks search button
    Then User checks that carousel list contain recently viewed product

    Examples:
      | mainUrl               | keyword  | keyword2 |
      | https://www.asos.com/ | 14072200 | 22603267 |


  Scenario Outline: Check navigation on products bar
    Given User opens '<mainUrl>' page
    And User clicks on shop women button
    When User moves to Topshop tab
    And User checks dresses link visibility on dropdown list
    And User moves to Face and Body tab
    And User checks View All brands link visibility
    And User moves to Brands tab
    Then User checks top brands visibility on dropdown list

    Examples:
      | mainUrl               |
      | https://www.asos.com/ |


  Scenario Outline: Check SIGN IN form
    Given User opens '<mainUrl>' page
    And User clicks on account button
    And User clicks on SignIn button
    And User checks email and password fields visibility
    And User checks SignIn button visibility
    And User checks that SignIn using socials are visible
    When User clicks on Forgot password button
    Then User checks Reset password visibility

    Examples:
      | mainUrl               |
      | https://www.asos.com/ |


  Scenario Outline: Check add product to cart
    Given User opens '<mainUrl>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks Add to bag button on product
    And User clicks on cart button
    When User clicks Checkout button
    Then User gets to the authorization page


    Examples:
      | mainUrl               | keyword  |
      | https://www.asos.com/ | 22343246 |


  Scenario Outline: Check 'Nothing matches your search'
    Given User opens '<mainUrl>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    Then User checks that title text is '<text>'

    Examples:
      | mainUrl               | keyword    | text                        |
      | https://www.asos.com/ | 1452512367 | NOTHING MATCHES YOUR SEARCH |


  Scenario Outline: Check SIGN IN with invalid login and password
    Given User opens '<mainUrl>' page
    And User clicks on account button
    And User clicks on SignIn button
    And User enter '<email>' on email field
    When User enter '<password>' on password field
    And User clicks on SignIn button on identity page
    Then User checks that '<error>' message appears

    Examples:
      | mainUrl               | email          | password | error                                 |
      | https://www.asos.com/ | test@gmail.com | qwerty11 | Sorry, we cannot log you in right now |


  Scenario Outline: Check add product to Saved Items
    Given User opens '<mainUrl>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks add to saved items on product
    Then User clicks on saved items button
    And User checks that product visibility on saved items

    Examples:
      | mainUrl               | keyword  |
      | https://www.asos.com/ | 22603267 |



















