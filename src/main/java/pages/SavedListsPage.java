package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SavedListsPage extends BasePage {

    @FindBy(xpath = "//a[contains(@href,'/prd/22603267?CTARef=Saved+Items+Image')]")
    private WebElement savedProductVisible;

    @FindBy(xpath = "//select[@id='sortBy']//option[text()=\"Recently added\"]")
    private WebElement recentlyAddedSort;

    public SavedListsPage(WebDriver driver) {
        super(driver);
    }

    public void isSavedProductVisible() {
        savedProductVisible.isDisplayed();
    }

    public WebElement getRecentlyAddedSort() {
        return recentlyAddedSort;
    }
}
