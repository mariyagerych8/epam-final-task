package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class IdentityPage extends BasePage {

    @FindBy(xpath = "//input[@id='EmailAddress']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='Password']")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id='signin']")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@id='signup-google']")
    private WebElement signInWithGoogleButton;

    @FindBy(xpath = "//a[@id='signup-apple']")
    private WebElement signInWithAppleButton;

    @FindBy(xpath = "//a[@id='signup-facebook']")
    private WebElement signInWithFacebookButton;

    @FindBy(xpath = "//a[@id='signup-twitter']")
    private WebElement signInWithTwitterButton;

    @FindBy(xpath = "//a[@data-st-tagname='id-signIn-forgotPassword-click']")
    private WebElement forgotPasswordButton;

    @FindBy(xpath = "//input[@value='Reset Password']")
    private WebElement resetPasswordButton;

    @FindBy(xpath = "//div[@class='error-block']//li[@id='loginErrorMessage']")
    private WebElement textWhenSendInvalidLoginAndPassword;

    public IdentityPage(WebDriver driver) {
        super(driver);
    }

    public boolean isEmailFieldVisible() {
        return emailField.isDisplayed();
    }

    public boolean isPasswordFieldVisible() {
        return passwordField.isDisplayed();
    }

    public boolean isSignInButtonVisible() {
        return signInButton.isDisplayed();
    }

    public boolean isSignInWithGoogleVisible() {
        return signInWithGoogleButton.isDisplayed();
    }

    public boolean isSignInWithAppleVisible() {
        return signInWithAppleButton.isDisplayed();
    }

    public boolean isSignInWithFacebookVisible() {
        return signInWithFacebookButton.isDisplayed();
    }

    public boolean isSignInWithTwitterVisible() {
        return signInWithTwitterButton.isDisplayed();
    }

    public void clickOnForgotPasswordButton() {
        forgotPasswordButton.click();
    }

    public boolean isResetPasswordButtonVisible() {
        return resetPasswordButton.isDisplayed();
    }

    public void clickOnEmailField() {
        emailField.click();
    }

    public void enterEmailToField(String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void clickOnPasswordField() {
        passwordField.click();
    }

    public void enterPasswordToField(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public String getTextWhenSendInvalidLoginAndPassword() {
        return textWhenSendInvalidLoginAndPassword.getText();
    }

    public void clickOnSignInButton() {
        signInButton.click();
    }
}
