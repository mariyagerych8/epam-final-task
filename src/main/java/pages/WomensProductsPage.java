package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class WomensProductsPage extends BasePage {

    @FindBy(xpath = "//nav[@class='_3EAPxMS']//button[@data-testid='primarynav-button']//span[text()=\"Topshop\"]")
    private WebElement topshopTab;

    @FindBy(xpath = "//div[@class='_2yreb5T _36PJqEn']//li[@class='_3QF73Dj']")
    private List<WebElement> topshopResultsList;

    @FindBy(xpath = "//div[@class='_2yreb5T _36PJqEn']//span[text()=\"Dresses\"]")
    private WebElement dressesLink;

    @FindBy(xpath = "//div[@class='_2yreb5T _36PJqEn']//span[text()=\"Top Brands\"]")
    private WebElement topBrands;

    @FindBy(xpath = "//div[@class='_2yreb5T _36PJqEn']//ul[@data-testid='secondarynav-flyout']")
    private WebElement dropdownListOnTabs;

    @FindBy(xpath = "//nav[@class='_3EAPxMS']//button[@data-testid='primarynav-button']//span[text()=\"Brands\"]")
    private WebElement brandTab;

    @FindBy(xpath = "//nav[@class='_3EAPxMS']//button[@data-testid='primarynav-button']//span[text()=\"Face + Body\"]")
    private WebElement faceAndBodyTab;

    @FindBy(xpath = "//a[contains(@href,\"ww|face+%2B+body|shop+by+brand|view+all+brands\")]")
    private WebElement viewAllFaceBodyBrandsLink;

    public WomensProductsPage(WebDriver driver) {
        super(driver);
    }

    public void clickTopshopTab() {
        Actions actions = new Actions(driver);
        actions.moveToElement(topshopTab).click().build().perform();
    }


    public boolean isDressesLinkVisible() {
        return dressesLink.isDisplayed();
    }

    public WebElement getDressesLink() {
        return dressesLink;
    }

    public boolean isTopBrandsVisible() {
        return topBrands.isDisplayed();
    }

    public WebElement getDropdownListOnTabs() {
        return dropdownListOnTabs;
    }

    public void clickBrandTab() {
        Actions actions = new Actions(driver);
        actions.moveToElement(brandTab).click().build().perform();
    }

    public void clickFaceAndBodyTab() {
        Actions actions = new Actions(driver);
        actions.moveToElement(faceAndBodyTab).click().build().perform();
    }

    public boolean isViewAllBrandsLinkVisible() {
        return viewAllFaceBodyBrandsLink.isDisplayed();
    }
}
