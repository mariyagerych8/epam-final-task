package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {


    @FindBy(xpath = "//span[@data-id='current-price']")
    private WebElement priceOfProduct;

    @FindBy(xpath = "//div[@class='fullImageContainer']/img[contains(@src,'22603267-1-bluestripe?$XXL')]")
    private WebElement imageOfProduct;

    @FindBy(xpath = "//div[@class='star-rating']")
    private WebElement starRating;

    @FindBy(xpath = "//label[@for='colourSelect']")
    private WebElement colourSelect;

    @FindBy(xpath = "//select[@data-id='sizeSelect']")
    private WebElement sizeSelect;

    @FindBy(xpath = "//button[@class='save-button']")
    private WebElement saveButton;

    @FindBy(xpath = "//button[@data-test-id='add-button']")
    private WebElement addToBagButton;

    @FindBy(xpath = "//a[contains(@href,'14072200?ctaref=recently+viewed')]")
    private WebElement firstProduct;

    @FindBy(xpath = "//a[contains(@href,'22603267?ctaref=recently+viewed')]")
    private WebElement secondProduct;

    @FindBy(xpath = "//section[contains(@class,'grid-backgroundWrapper')]//h2[@class='grid-text__title ']")
    private WebElement titleTextWhenNoResults;

    @FindBy(xpath = "//a[@href='https://www.asos.com/prd/14525123']")
    private WebElement productOnBag;

    @FindBy(xpath = "//a[@href='https://www.asos.com/customer-service/delivery/'] ")
    private WebElement deliveryInfoButton;

    @FindBy(xpath = "//a[contains(@class,'checkout bag-total-button--checkout--total')]")
    private WebElement checkoutButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public boolean isPriceOfProductVisible() {
        return priceOfProduct.isDisplayed();
    }

    public boolean isImageOfProductVisible() {
        return imageOfProduct.isDisplayed();
    }

    public boolean isStarRatingVisible() {
        return starRating.isDisplayed();
    }

    public boolean isColourSelectVisible() {
        return colourSelect.isDisplayed();
    }

    public boolean isSizeSelectVisible() {
        return sizeSelect.isDisplayed();
    }

    public boolean isSaveButtonVisible() {
        return saveButton.isDisplayed();
    }


    public boolean isAddToBagButtonVisible() {
        return addToBagButton.isDisplayed();
    }

    public boolean isFirstProductVisible() {
        return firstProduct.isDisplayed();
    }

    public boolean isSecondVisible() {
        return secondProduct.isDisplayed();
    }


    public String getTitleTextWhenNoResults() {
        return titleTextWhenNoResults.getText();
    }

    public void clickAddToBagButton() {
        addToBagButton.click();
    }

    public boolean productVisibilityOnBag() {
        return productOnBag.isDisplayed();
    }

    public boolean moreDeliveryInfoVisible() {
        Actions actions = new Actions(driver);
        actions.moveToElement(deliveryInfoButton).build().perform();
        return deliveryInfoButton.isDisplayed();
    }

    public void clickCheckoutButton() {
        checkoutButton.click();
    }

    public void clickSaveButton() {
        saveButton.click();
    }
}
