package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//li[@data-dropdown-id='sort']//button[@aria-haspopup='listbox']")
    private WebElement sortDropdownList;

    @FindBy(xpath = "//ul[@data-test-id='sortOptions']//li[@id='plp_web_sort_price_high_to_low']")
    private WebElement priceHightToLowOption;

    @FindBy(xpath = "//li[@data-dropdown-id='floor']//button[@class='_1om7l06']")
    private WebElement floorDropdownList;

    @FindBy(xpath = "//div[@class='_2WeosbA']//label[@for='floor_2001']")
    private WebElement unisexOption;

    @FindBy(xpath = "//li[@data-dropdown-id='base_colour']//button[@class='_1om7l06']")
    private WebElement colorDropdownList;

    @FindBy(xpath = "//div[@class='_2WeosbA']//label[@for='base_colour_4']")
    private WebElement blackOption;


    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnSortDropdownList() {
        sortDropdownList.click();
    }


    public void clickOnPriceHightToLowOption() {
        priceHightToLowOption.click();
    }

    public void clickOnFloorDropdownList() {
        floorDropdownList.click();
    }

    public void clickOnUnisexOption() {
        unisexOption.click();
    }

    public void clickOnColorDropdownList() {
        colorDropdownList.click();
    }

    public void clickOnBlackOption() {
        blackOption.click();
    }
}
