package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//a[@data-testid='asoslogo']")
    private WebElement mainLogo;

    @FindBy(xpath = "//div[@data-testid='header']")
    private WebElement header;

    @FindBy(xpath = "//div[@data-testid='topbar']")
    private WebElement topbar;

    @FindBy(xpath = "//footer[@data-testid='footer']")
    private WebElement footer;

    @FindBy(xpath = "//input[@data-testid='search-input']")
    private WebElement searchField;

    @FindBy(xpath = "//a[@href='/women/?ctaref=HP|gen|top|women']")
    private WebElement shopWomenButton;

    @FindBy(xpath = "//a[@href='/men/?ctaref=HP|gen|top|men']")
    private WebElement shopManButton;

    @FindBy(xpath = "//button[@data-testid='myAccountIcon']")
    private WebElement accountButton;

    @FindBy(xpath = "//a[@data-testid='miniBagIcon']")
    private WebElement cartButton;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement savedItemButton;

    @FindBy(xpath = "//div[@id='chrome-header']//button[@data-testid='country-selector-btn']")
    private WebElement countrySelectorn;

    @FindBy(xpath = "//a[@data-testid='signin-link']")
    private WebElement signInLink;

    @FindBy(xpath = "//a[@data-testid='signup-link']")
    private WebElement signUpLink;

    @FindBy(xpath = "//a[@data-testid='myaccount-link']")
    private WebElement myAccountLink;

    @FindBy(xpath = "//a[@data-testid='myorders-link']")
    private WebElement myOrdersLink;

    @FindBy(xpath = "//a[@data-testid='returnsinformation-link']")
    private WebElement returnsInformationLink;

    @FindBy(xpath = "//a[@data-testid='contactpreferences-link']")
    private WebElement contactPreferencesLink;

    @FindBy(xpath = "//button[@data-testid='search-button-inline']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@data-test-id='bag-link']")
    private WebElement viewBagButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public void isMainLogoVisible() {
        mainLogo.isDisplayed();
    }


    public void isHeaderVisible() {
        header.isDisplayed();
    }

    public void isTopbarVisibile() {
        topbar.isDisplayed();
    }

    public void isFooterVisible() {
        footer.isDisplayed();
    }

    public void isSearchFieldVisible() {
        searchField.isDisplayed();
    }

    public void isShopWomenButtonVisible() {
        shopWomenButton.isDisplayed();

    }

    public void isShopManButtonVisible() {
        shopManButton.isDisplayed();
    }

    public void isAccountButtonVisible() {
        accountButton.isDisplayed();

    }

    public void isCartButtonVisible() {
        cartButton.isDisplayed();
    }

    public void isSavedItemsButtonVisible() {
        savedItemButton.isDisplayed();
    }

    public void isCountrySelectorVisible() {
        countrySelectorn.isDisplayed();
    }

    public void clickAccountButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(accountButton).click().build().perform();
    }

    public boolean isSignInLinkVisible() {
        return signInLink.isDisplayed();
    }

    public boolean isSignUpLinkVisible() {
        return signUpLink.isDisplayed();
    }

    public boolean isMyAccountLinkVisible() {
        return myAccountLink.isDisplayed();
    }

    public boolean isMyOrdersLinkVisible() {
        return myOrdersLink.isDisplayed();
    }

    public boolean isReturnsInformationLinkVisible() {
        return returnsInformationLink.isDisplayed();
    }

    public boolean isContactPreferencesLinkVisible() {
        return contactPreferencesLink.isDisplayed();
    }

    public void clickShopWomenButton() {
        shopWomenButton.click();
    }

    public void clickSearchField() {
        searchField.click();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void clickOnSignInLinkVisible() {
        signInLink.click();
    }

    public void clickCartButton() {
        cartButton.click();
    }

    public void clickViewBagButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(viewBagButton).click().build().perform();
    }

    public void clickSavedItemsButton() {
        savedItemButton.click();
    }
}
