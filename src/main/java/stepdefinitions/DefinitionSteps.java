package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DefinitionSteps {

    WebDriver driver;
    HomePage homePage;
    PageFactoryManager pageFactoryManager;
    WomensProductsPage womensProductsPage;
    ProductPage productPage;
    IdentityPage identityPage;
    SearchResultPage searchResultPage;
    SavedListsPage savedListsPage;
    private static final long DEFAULT_TIMEOUT = 60;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void userOpensHomePagePage(String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @And("User checks main logo visibility")
    public void userChecksMainLogoVisibility() {
        homePage.isMainLogoVisible();
    }

    @And("User checks header visibility")
    public void userChecksHeaderVisibility() {
        homePage.isHeaderVisible();
    }

    @And("User checks topbar visibility")
    public void userChecksTopbarVisibility() {
        homePage.isTopbarVisibile();
    }

    @And("User checks footer visibility")
    public void userChecksFooterVisibility() {
        homePage.isFooterVisible();
    }

    @And("User checks search field visibility")
    public void userChecksSearchFieldVisibility() {
        homePage.isSearchFieldVisible();
    }

    @And("User checks shop women button visibility")
    public void userChecksShopWomenButtonVisibility() {
        homePage.isShopWomenButtonVisible();
    }

    @And("User checks shop men button visibility")
    public void userChecksShopMenButtonVisibility() {
        homePage.isShopManButtonVisible();
    }

    @And("User checks account button visibility")
    public void userChecksAccountButtonVisibility() {
        homePage.isAccountButtonVisible();

    }

    @And("User checks cart visibility")
    public void userChecksCartVisibility() {
        homePage.isCartButtonVisible();
    }

    @And("User checks saved Items icon visibility")
    public void userChecksSavedItemsIconVisibility() {
        homePage.isSavedItemsButtonVisible();
    }

    @And("User checks country selector visibility")
    public void userChecksCountrySelectorVisibility() {
        homePage.isCountrySelectorVisible();
    }

    @When("User moves to Account button")
    public void userClicksAccountButton() {
        homePage.clickAccountButton();
    }

    @And("User checks all items visibility on dropdown list")
    public void userChecksAllItemsVisibilityOnDropdownList() {
        assertTrue(homePage.isSignInLinkVisible());
        assertTrue(homePage.isSignUpLinkVisible());
        assertTrue(homePage.isMyAccountLinkVisible());
        assertTrue(homePage.isMyOrdersLinkVisible());
        assertTrue(homePage.isReturnsInformationLinkVisible());
        assertTrue(homePage.isContactPreferencesLinkVisible());
    }

    @And("User clicks on shop women button")
    public void userClicksOnShopWomenButton() {
        homePage.clickShopWomenButton();
    }

    @Then("User checks that current url contains {string}")
    public void userChecksThatCurrentUrlContainsKeyword(final String floor) {
        assertTrue(driver.getCurrentUrl().contains(floor));
    }


    @When("User moves to Topshop tab")
    public void userMovesToTopshopTab() {
        womensProductsPage = pageFactoryManager.getWomensProductsPage();
        womensProductsPage.clickTopshopTab();
    }


    @Then("User checks dresses link visibility on dropdown list")
    public void userChecksDressesLinkVisibilityOnDropdownList() {
        womensProductsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, womensProductsPage.getDressesLink());
        assertTrue(womensProductsPage.isDressesLinkVisible());
    }

    @And("User moves to Brands tab")
    public void userMovesToBrandsTab() {
        womensProductsPage.clickBrandTab();
    }

    @And("User checks top brands visibility on dropdown list")
    public void userChecksTopBrandsVisibilityOnDropdownList() {
        womensProductsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, womensProductsPage.getDropdownListOnTabs());
        assertTrue(womensProductsPage.isTopBrandsVisible());
    }

    @And("User moves to Face and Body tab")
    public void userMovesToFaceAndBodyTab() {
        womensProductsPage.clickFaceAndBodyTab();
    }

    @And("User checks View All brands link visibility")
    public void userChecksViewAllBrandsLinkVisibility() {
        womensProductsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, womensProductsPage.getDropdownListOnTabs());
        assertTrue(womensProductsPage.isViewAllBrandsLinkVisible());
    }

    @When("User clicks on search field")
    public void userClicksOnSearchField() {
        homePage.clickSearchField();
    }

    @Then("User makes search by keyword {string}")
    public void userMakesSearchByKeywordKeyword(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User checks Price of product visibility")
    public void userChecksPriceOfProductVisibility() {
        productPage = pageFactoryManager.getProductPage();
        assertTrue(productPage.isPriceOfProductVisible());
    }

    @And("User clicks search button")
    public void userClicksSearchButton() {
        homePage.clickSearchButton();
    }

    @And("User checks Image of product visibility")
    public void userChecksImageOfProductVisibility() {
        assertTrue(productPage.isImageOfProductVisible());

    }

    @And("User checks Star rating visibility")
    public void userChecksStarRatingVisibility() {
        assertTrue(productPage.isStarRatingVisible());
    }

    @Then("User checks Colour select visibility")
    public void userChecksColourSelectVisibility() {
        assertTrue(productPage.isColourSelectVisible());
    }

    @And("User checks Size select visibility")
    public void userChecksSizeSelectVisibility() {
        assertTrue(productPage.isSizeSelectVisible());
    }

    @And("User checks Save button visibility")
    public void userChecksSaveButtonVisibility() {
        assertTrue(productPage.isSaveButtonVisible());
    }

    @And("User checks Add to bag button visibility")
    public void userChecksAddToBagButtonVisibility() {
        assertTrue(productPage.isAddToBagButtonVisible());
    }

    @Then("User checks that carousel list contain recently viewed product")
    public void userChecksThatCarouselListContainRecentlyViewedProduct() {
        productPage = pageFactoryManager.getProductPage();
        productPage.implicitWait(30);
        assertTrue(productPage.isFirstProductVisible());
    }

    @And("User clicks on account button")
    public void userClicksOnAccountButton() {
        homePage.clickAccountButton();
    }

    @And("User clicks on SignIn button")
    public void userClicksOnSignInButton() {
        homePage.clickOnSignInLinkVisible();
    }

    @And("User checks email and password fields visibility")
    public void userChecksEmailAndPasswordFieldsVisibility() {
        identityPage = pageFactoryManager.getIdentityPage();
        assertTrue(identityPage.isEmailFieldVisible());
        assertTrue(identityPage.isPasswordFieldVisible());
    }

    @And("User checks SignIn button visibility")
    public void userChecksSignInButtonVisibility() {
        assertTrue(identityPage.isSignInButtonVisible());
    }


    @And("User checks that SignIn using socials are visible")
    public void userChecksThatSignInUsingSocialsAreVisible() {
        assertTrue(identityPage.isSignInWithGoogleVisible());
        assertTrue(identityPage.isSignInWithAppleVisible());
        assertTrue(identityPage.isSignInWithFacebookVisible());
        assertTrue(identityPage.isSignInWithTwitterVisible());
    }


    @And("User clicks on Forgot password button")
    public void userClicksOnForgotPasswordButton() {
        identityPage.clickOnForgotPasswordButton();
    }

    @Then("User checks Reset password visibility")
    public void userChecksResetPasswordVisibility() {
        assertTrue(identityPage.isResetPasswordButtonVisible());
    }


    @And("User checks that title text is {string}")
    public void userChecksThatTitleTextIsText(final String expectedText) {
        productPage = pageFactoryManager.getProductPage();
        assertEquals(productPage.getTitleTextWhenNoResults(), expectedText);
    }

    @And("User clicks Add to bag button on product")
    public void userClicksAddToBagButtonOnProduct() {
        productPage = pageFactoryManager.getProductPage();
        productPage.clickAddToBagButton();
    }

    @And("User clicks on cart button")
    public void userClicksOnCartButton() {
        productPage.implicitWait(30);
        homePage.clickCartButton();
    }

    @And("user clicks View bag button")
    public void userClicksViewBagButton() {
        homePage.clickViewBagButton();
    }

    @And("User checks product visibility on bag")
    public void userChecksProductVisibilityOnBag() {
        assertTrue(productPage.productVisibilityOnBag());

    }

    @And("User checks More delivery info visibility")
    public void userChecksMoreDeliveryInfoVisibility() {
        assertTrue(productPage.moreDeliveryInfoVisible());
    }

    @When("User clicks Checkout button")
    public void userClicksCheckoutButton() {
        productPage.implicitWait(30);
        productPage.clickCheckoutButton();
    }

    @Then("User gets to the authorization page")
    public void userGetsToTheAuthorizationPage() {
        identityPage = pageFactoryManager.getIdentityPage();
        identityPage.isEmailFieldVisible();
    }

    @And("User sorts products by Price high to low")
    public void userSortsProductsByPriceHighToLow() {
        searchResultPage = pageFactoryManager.getSearchResultPage();
        searchResultPage.clickOnSortDropdownList();
        searchResultPage.clickOnPriceHightToLowOption();

    }

    @And("user selects a Gender from the drop-down list")
    public void userSelectsAGenderFromTheDropDownList() {
        searchResultPage.clickOnFloorDropdownList();
        searchResultPage.clickOnUnisexOption();
    }

    @And("User selects a Color from the drop-down list")
    public void userSelectsAColorFromTheDropDownList() {
        searchResultPage.clickOnColorDropdownList();
        searchResultPage.clickOnBlackOption();
    }


    @And("User enter {string} on email field")
    public void userEnterEmailOnEmailField(final String email) {
        identityPage = pageFactoryManager.getIdentityPage();
        identityPage.clickOnEmailField();
        identityPage.enterEmailToField(email);
    }

    @When("User enter {string} on password field")
    public void userEnterPasswordOnPasswordField(final String password) {
        identityPage.clickOnPasswordField();
        identityPage.enterPasswordToField(password);
    }

    @Then("User checks that {string} message appears")
    public void userChecksThatErrorMessageAppears(final String errorMessage) {
        assertEquals(identityPage.getTextWhenSendInvalidLoginAndPassword(), errorMessage);

    }

    @And("User clicks on SignIn button on identity page")
    public void userClicksOnSignInButtonOnIdentityPage() {
        identityPage.clickOnSignInButton();
    }

    @And("User clicks add to saved items on product")
    public void userClicksAddToSavedItemsOnProduct() {
        productPage = pageFactoryManager.getProductPage();
        productPage.clickSaveButton();
    }

    @Then("User clicks on saved items button")
    public void userClicksOnSavedItemsButton() {
        homePage.clickSavedItemsButton();
    }

    @And("User checks that product visibility on saved items")
    public void userChecksThatProductVisibilityOnSavedItems() {
        savedListsPage = pageFactoryManager.getSavedListsPage();
        savedListsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, savedListsPage.getRecentlyAddedSort());
        savedListsPage.isSavedProductVisible();
    }
}
